" reduce, reuse, recycle
" feel free to copy, adapt, share

" use Vim settings, rather than Vi settings
" must be at top!
set nocompatible

" map leader key to comma
let mapleader = ","

" enable file type detection, use filetype settings
" including indenting rules
filetype plugin indent on


"
" general settings
"

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set ruler                 " show the cursor position in status line
set showcmd               " display incomplete commands
set wildmenu              " display completion matches in status line
set wildmode=list:longest " complete to longest common match, show list
set number                " show line numbers
set laststatus=2          " always show statusline
set noshowmode            " don't show mode, in combination with airline
set matchpairs+=<:>       " add < > to bracket pair matching
set spelllang=en_us       " set language for spell checking
set cursorcolumn          " highlight column of cursor (vertical)
set cursorline            " highlight line of cursor (horizontal)

" edit and source vimrc
nnoremap <leader>ve :vsplit ~/.vimrc<CR>
nnoremap <leader>vs :source ~/.vimrc<CR>

" write, quit
nnoremap <leader>w :write<CR>
vnoremap <leader>w <ESC>:write<CR>
nnoremap <leader>q :quit<CR>
vnoremap <leader>q <ESC>:quit<CR>


"
" buffer settings
"

" list buffers and prepare for switch
nnoremap <leader>b :buffers<CR>:buffer<space>
" more mappings
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprev<CR>
nnoremap <leader>bd :bdelete<CR>
nnoremap <leader>bda :%bdelete<CR>


"
" indenting
"

set shiftwidth=2  " amount of whitespaces per level of indentation
set softtabstop=2 " amount of whitespaces per tab and backspace
set expandtab     " no more tabs (\t), use spaces instead

autocmd FileType sh setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 expandtab

"
" search, replace, highlight
"
set hlsearch    " enable search highlighting
set incsearch   " incremental search
set ignorecase  " ignore case by default
set smartcase   " become case sensitive if has upper characters

" search and replace word under cursor with new name
nnoremap <leader>* :%s/<c-r><c-w>/<c-r><c-w>/gc<Left><Left><Left>
" stop highlighting of search
nnoremap <leader>h :nohlsearch<CR>


"
" optional packages
"

" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
packadd! matchit
endif


"
" autocommands
"

" remove that trailing whitespace bullshit.
autocmd BufWritePre * :%s/\s\+$//e

" let's at least combat grammer in files where it might matter the most.
autocmd FileType markdown set spell


"
" plugins
"

" install vim-plug automatically on initial setup
" see https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" call plugins
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
call plug#end()

"
" CtrlP
"
nnoremap <leader>p :CtrlP<space>
let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux

"
" syntax highlighting, colors
"

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

syntax on
colorscheme onehalfdark

"
" Airline
"
let g:airline_theme='onehalfdark'
