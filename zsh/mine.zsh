export PATH=$PATH:/usr/share/darktable/tools/

### Alias ###
alias clip='xclip -selection clipboard'
alias igrep='grep -i'

alias github='cd ~/git/github'
alias gitlab='cd ~/git/gitlab'
alias dotfiles='cd ~/git/gitlab/endeavour-dotfiles'


alias ez='vim ~/.zshrc'
alias ezm='vim ~/.oh-my-zsh/custom/mine.zsh'
alias sz='source ~/.zshrc'

alias lower='tr "[:upper:]" "[:lower:]"'
alias upper='tr "[:lower:]" "[:upper:]"'

alias bup='rsync -av ${HOME}/data /run/media/alex/backup/ --delete'
